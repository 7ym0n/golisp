(defun sqrt-iter (guess x)
  (cond ((good-enough? guess x) guess)
	(T (sqrt-iter (improve guess x) x))))

(defun improve (guess x)
  (average guess (/ x guess)))

(defun average (x y)
  (/ (+ x y) 2))

(defun good-enough? (guess x)
  (< (abs (- (square guess) x)) 0.001))

(defun abs (x)
  (cond ((< x 0) (- x))
	(T x)))

(defun sqrt (x)
  (sqrt-iter 1.0 x))

(defun square (x)
  (* x x))

(display (sqrt 9))
