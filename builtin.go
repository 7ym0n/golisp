/* 
defines Built-in functions 

Built-in functions should
 * return 'expression' as a single return value.
 * use constNil, constT for boolean result
*/
package main

import (
	"fmt"
)

func atom(e expression) expression {
	if isAtom(e) {
		return constT
	}
	return constNil
}

func cons(e1, e2 expression) expression {
	return &Cons{e1, e2}
}

func cadddr(exp expression) expression {
	return car(cdr(cdr(cdr(exp))))
}

func caddr(exp expression) expression {
	return car(cdr(cdr(exp)))
}

func cadr(exp expression) expression {
	return car(cdr(exp))
}

func cddr(exp expression) expression {
	return cdr(cdr(exp))
}

func caar(exp expression) expression {
	return car(car(exp))
}

func cadar(exp expression) expression {
	return car(cdr(car(exp)))
}

func caddar(exp expression) expression {
	return car(cdr(cdr(car(exp))))
}

func car(exp expression) expression {
	c, ok := exp.(*Cons)
	if !ok || c == nil {
		panic("Runtime Error -- car should be applied to cons : " + print(exp))
	}
	return c.car
}

func cdr(exp expression) expression {
	c, ok := exp.(*Cons)
	if !ok || c == nil {
		panic("Runtime Error -- cdr should be applied to cons : " + print(exp))
	}
	return c.cdr
}

func list(exp ...expression) expression {
	if len(exp) == 0 {
		return constNil
	}
	return cons(exp[0], list(exp[1:]...))
}

func equal(e1, e2 expression) expression {
	if equals(e1, e2) {
		return constT
	}
	return constNil
}

func builtinAdd(a, b expression) expression {
	return numValue(a) + numValue(b)
}

func builtinSub(a, b expression) expression {
	return numValue(a) - numValue(b)
}

func builtinMul(a, b expression) expression {
	return numValue(a) * numValue(b)
}

func builtinDiv(a, b expression) expression {
	return numValue(a) / numValue(b)
}

func builtinEq(a, b expression) expression {
	if numValue(a) == numValue(b) {
		return constT
	}
	return constNil
}

func builtinLesserThan(a, b expression) expression {
	if numValue(a) < numValue(b) {
		return constT
	}
	return constNil
}

func builtinGreaterThan(a, b expression) expression {
	if numValue(a) > numValue(b) {
		return constT
	}
	return constNil
}

func builtinPrint(a expression) expression {
	fmt.Print(print(a))
	return a
}

func builtinDisplay(a expression) expression {
	fmt.Print(a)
	return a
}
