package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"reflect"
	"regexp"
	"strconv"
)

var (
	constT   expression = Symbol("T")
	constNil *Cons      = nil
)

type expression interface{}
type Symbol string
type Cons struct {
	car, cdr expression
}

func isNil(e expression) bool {
	if e == nil {
		return true
	}
	c, ok := e.(*Cons)
	return ok && c == nil
}

func (c *Cons) String() string {
	var buf bytes.Buffer
	buf.WriteString("(")
	var cur expression = c
	for !isNil(cur) {
		pair, ok := cur.(*Cons)
		if ok {
			if c != cur {
				buf.WriteString(" ")
			}
			buf.WriteString(print(pair.car))
			cur = pair.cdr
		} else {
			buf.WriteString(fmt.Sprintf(" . %v", cur))
			break
		}
	}
	buf.WriteString(")")
	return buf.String()
}

// null or not pair
func isAtom(e expression) bool {
	return !isPair(e)
}

// *Cons but not null
func isPair(e expression) bool {
	c, ok := e.(*Cons)
	return ok && c != nil
}

func isSymbol(e expression) bool {
	_, ok := e.(Symbol)
	return ok
}

func equals(e1, e2 expression) bool {
	return print(e1) == print(e2)
}

// (cond (p1 e1) (p2 e2) ... )
// => eval(en) if eval(pn) is the first non-NIL 
func evalCond(cnd expression, env map[string]expression) (expression, map[string]expression) {
	for ; !isNil(cnd); cnd = cdr(cnd) {
		if !isNil(eval(caar(cnd), env)) {
			return cadar(cnd), env
		}
	}
	panic("Runtime error: cond -- all predicates evaluates to NIL")
}

func variable(e expression, env map[string]expression) expression {
	if v, ok := env[print(e)]; ok {
		return v
	}
	if isCxr(e) {
		return e
	}
	panic("Runtime error: variable -- undefined: " + print(e))
}

func copyEnv(env map[string]expression) map[string]expression {
	newEnv := make(map[string]expression)
	for k, v := range env {
		newEnv[k] = v
	}
	return newEnv
}

func isPrimitive(e expression) bool {
	v := reflect.ValueOf(e)
	return v.Kind() == reflect.Func
}

func fromList(args expression) (result []reflect.Value) {
	for ; !isNil(args); args = cdr(args) {
		result = append(result, reflect.ValueOf(car(args)))
	}
	return result
}

func applyPrimitive(fn expression, args expression) expression {
	result := reflect.ValueOf(fn).Call(fromList(args))
	return result[0].Interface()
}

func extend(env map[string]expression, name, value expression) map[string]expression {
	newEnv := copyEnv(env)
	newEnv[print(name)] = value
	return newEnv
}

func extend_n(env map[string]expression, names, values expression) map[string]expression {
	newEnv := copyEnv(env)
	for ; !isNil(names); names, values = cdr(names), cdr(values) {
		if isAtom(names) {
			newEnv[print(names)] = values
			break
		}
		newEnv[print(car(names))] = car(values)
	}
	return newEnv
}

func isCompound(fn expression) bool {
	return isPair(fn)
}

func applyCompound(fn, args expression, env map[string]expression) (expression, map[string]expression) {
	if TRACE {
		fmt.Printf("APPLY: %v -- %v\n", print(fn), print(args))
	}
	for car(fn) != Symbol("lambda") {
		switch print(car(fn)) {
		case "label":
			fn, args, env = caddr(fn), args, extend(env, cadr(fn), caddr(fn))
		case "funarg":
			fn, args, env = cadr(fn), args, caddr(fn).(map[string]expression)
		default:
			panic("Runtime Error - unknown application : " + print(car(fn)))
		}
	}
	return caddr(fn), extend_n(env, cadr(fn), args)
}

// c[ad]+r
func isCxr(name expression) bool {
	return regexp.MustCompile("^c[ad]+r$").MatchString(print(name))
}

func applyCxr(cxr, x expression) expression {
	s := print(cxr)
	for i := len(s) - 2; i > 0; i-- {
		if s[i] == 'a' {
			x = car(x)
		} else {
			x = cdr(x)
		}
	}
	return x
}

func evalList(exp expression, env map[string]expression) expression {
	if isNil(exp) {
		return constNil
	}
	return cons(eval(car(exp), env), evalList(cdr(exp), env))
}

// (defun fname (v1 v2 ..) e) 
// = fname with env updated with new association {fname: (label fname (lambda (v1 v2 ..) e))}
func evalDefun(exp expression, env map[string]expression) expression {
	fname := cadr(exp)
	env[print(fname)] = convertDefToLambda(exp)
	return fname
}

// TODO: using parse/sprintf is ugly but works for a while
// will be replaced with macro feature
func convertDefToLambda(exp expression) expression {
	exp, ok := Parse(fmt.Sprintf("(label %s (lambda %s %s))", cadr(exp), caddr(exp), cadddr(exp)))
	if ok {
		return exp
	}
	panic("should not reach here")
}

// (defmacro name (form) e)
// = name with env updated with new macro {name: (label name (lambda (form) e))}
func evalDefMacro(exp expression, env map[string]expression) expression {
	fname := cadr(exp)
	env[print(fname)+" MACRO"] = convertDefToLambda(exp)
	return fname
}

func maplist(l expression, fn func(expression) expression) expression {
	if isNil(l) {
		return constNil
	}
	return cons(fn(l), maplist(cdr(l), fn))
}

// macro expansion 
func expand(exp expression, env map[string]expression) (result expression) {
	if TRACE {
		fmt.Printf("expand: %v\n", print(exp))
	}
	defer func() {
		if TRACE {
			fmt.Printf("===> %v\n", print(result))
		}
	}()
	if isAtom(exp) {
		return exp
	}
	if isAtom(car(exp)) {
		switch print(car(exp)) {
		case "quote":
			return exp
		case "lambda", "label":
			return list(car(exp), cadr(exp), expand(caddr(exp), env))
		case "defun", "defmacro":
			return list(car(exp), cadr(exp), caddr(exp), expand(cadddr(exp), env))
		}
		if v, ok := env[print(car(exp))+" MACRO"]; ok {
			return expand(eval(applyCompound(v, list(exp), env)), env)
		}
	}
	return maplist(exp, func(l expression) expression { return expand(car(l), env) })
}

var TRACE = false
var depthOfEval = 0

func eval(exp expression, env map[string]expression) expression {
	if TRACE {
		fmt.Printf("EVAL(%v): %v\n", depthOfEval, print(exp))
		depthOfEval++
		defer func() {
			depthOfEval--
		}()
	} else {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println(print(exp))
				panic(r)
			}
		}()
	}

EvalLoop:
	for {
		if isNil(exp) {
			panic("Runtime Error -- nil can't be evaluated")
		}
		if isAtom(exp) {
			if isSymbol(exp) {
				if TRACE {
					fmt.Println(print(variable(exp, env)))
				}
				return variable(exp, env)
			}
			return exp // string, number
		}
		if f := car(exp); isAtom(f) {
			switch print(f) {
			case "quote":
				return cadr(exp)
			case "function": // functional arguments capture env
				return list(Symbol("funarg"), cadr(exp), env)
			case "lambda", "label": // adopt recent style of lambda expression
				return exp // dynamic scope for free variables
			case "cond":
				exp, env = evalCond(cdr(exp), env)
				continue EvalLoop
			case "defun":
				return evalDefun(exp, env)
			case "defmacro":
				return evalDefMacro(exp, env)
			}
		}
		fn := eval(car(exp), env)
		args := evalList(cdr(exp), env)
		if isPrimitive(fn) {
			return applyPrimitive(fn, args)
		}
		if isCxr(fn) { // car, cdr, cadr, ... as primitives
			return applyCxr(fn, car(args))
		}
		if isCompound(fn) {
			exp, env = applyCompound(fn, args, env)
		}
	}
	panic("not reach here")
}

func numValue(e expression) float64 {
	switch e.(type) {
	case int:
		return float64(e.(int))
	case float64:
		return e.(float64)
	}
	panic("Runtime Error -- can't convert to number : " + print(e))
}

func defaultEnv() map[string]expression {
	env := make(map[string]expression)
	env["T"] = constT
	env["NIL"] = constNil
	env["car"] = car
	env["cdr"] = cdr
	env["cons"] = cons
	env["equal"] = equal
	env["atom"] = atom
	env["list"] = list
	env["builtin.add"] = builtinAdd
	env["builtin.sub"] = builtinSub
	env["builtin.mul"] = builtinMul
	env["builtin.div"] = builtinDiv
	env["builtin.lt"] = builtinLesserThan
	env["builtin.gt"] = builtinGreaterThan
	env["builtin.eq"] = builtinEq
	env["print"] = builtinPrint
	env["display"] = builtinDisplay

	loadFile("init.lisp", env)

	return env
}

func evalString(s string, env map[string]expression) string {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(r)
		}
	}()
	e, ok := Parse(s)
	if !ok {
		panic("evalString() -- failed : " + s)
	}
	r := eval(expand(e, env), env)
	return print(r)
}

func loadFile(s string, env map[string]expression) string {
	f, err := os.Open(s)
	if err != nil {
		panic("loadFile failed")
	}
	return load(f, env)
}

func load(r io.Reader, env map[string]expression) string {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		panic("load failed")
	}
	return loadString(string(data), env)
}

func loadString(s string, env map[string]expression) string {
	//	defer func() {
	//		if r := recover(); r != nil {
	//			fmt.Println(r)
	//		}
	//	}()
	last := ""
	var p parser
	p.init(s)
	for {
		e, ok := p.parseExpression()
		if !ok {
			break
		}
		last = print(eval(expand(e, env), env))
	}
	return last

}

func print(exp expression) string {
	if exp == nil {
		return "()"
	}

	switch exp.(type) {
	case *Cons:
		return exp.(*Cons).String()
	case string:
		return strconv.Quote(exp.(string))
	default:
		return fmt.Sprintf("%v", exp)
	}
	panic("Should not reach here")
}

type LispEvaluator struct {
	env map[string]expression
}

func (_ LispEvaluator) parse(s string) (interface{}, bool) {
	return Parse(s)
}

func (le LispEvaluator) eval(exp interface{}) interface{} {
	return eval(expand(exp.(expression), le.env), le.env)
}

func (_ LispEvaluator) print(result interface{}) string {
	i, _ := result.(expression)
	return print(i)
}

var file string

func main() {
	flag.BoolVar(&TRACE, "t", false, "traces eval(depth)")
	flag.StringVar(&file, "f", "", "runs file")
	flag.Parse()
	if !TRACE {
		defer func() {
			if x := recover(); x != nil {
				fmt.Println(x)
			}
		}()
	}

	if file == "" {
		v := Repl{e: LispEvaluator{env: defaultEnv()}}
		v.loop()
	} else {
		loadFile(file, defaultEnv())
	}
}
