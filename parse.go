// ref: http://www.schemers.org/Documents/Standards/R5RS/HTML/r5rs-Z-H-10.html#%_sec_7.1.2
package main

import (
	"strconv"
	"strings"
)

type IncompleteExpression string

type parser struct {
	s Scanner

	// next token
	tok rune
	lit string
}

func Parse(data string) (e expression, b bool) {
	defer func() {
		if r := recover(); r != nil {
			if _, ok := r.(IncompleteExpression); ok {
				e, b = nil, false
			} else {
				panic(r)
			}
		}
	}()
	var p parser
	p.init(data)
	e, b = p.parseExpression()
	return
}

// atom : symbol(identifier) | number | quoted-string
// symbol : letter+
// number : ...
// expression : atom or list or '(quote)
// list : ( expression ... )
func (p *parser) init(data string) {
	p.s.Init(strings.NewReader(data))
	p.next()
}

func (p *parser) parseExpression() (expression, bool) {
	switch p.tok {
	case '(':
		return p.parseList()
	case '\'':
		return p.parseQuote()
	default:
		return p.parseAtom()
	}
	return "cannot reach here", false
}

// list := ( expression* )
//      |  ( expression+ . expression )
//      |  ' expression                 ; handled by parseExpression
func (p *parser) parseList() (expression, bool) {
	p.expect('(')

	var head, cur *Cons
	for p.tok != EOF && p.tok != ')' && p.tok != '.' {
		e, ok := p.parseExpression()
		if !ok {
			return "parseExpression failed", false
		}
		if head == nil {
			head = &Cons{e, constNil}
			cur = head
		} else {
			new := &Cons{e, constNil}
			cur.cdr = new
			cur = new
		}
	}

	if p.tok == '.' {
		p.next()
		e, ok := p.parseExpression()
		if !ok {
			return "parseExpression failed", false
		}
		cur.cdr = e
	}

	p.expect(')')
	return head, true
}

// 'a, '(a b c)
func (p *parser) parseQuote() (expression, bool) {
	p.next()
	quoted, ok := p.parseExpression()
	if ok {
		return cons(Symbol("quote"), cons(quoted, constNil)), true
	}
	return nil, false
}

// atom := identifier | number | string
func (p *parser) parseAtom() (expression, bool) {
	tok := p.tok
	lit := p.lit
	p.next()
	switch tok {
	case Ident:
		return Symbol(lit), true
	case Int:
		i, err := strconv.Atoi(lit)
		if err != nil {
			return "Invalid num format", false
		}
		return i, true
	case Float:
		f, err := strconv.Atof64(lit)
		if err != nil {
			return "Invalid num format", false
		}
		return f, true
	case String:
		s, err := strconv.Unquote(lit)
		if err != nil {
			return "Invalid string format", false
		}
		return s, true
	}
	return "Atom should be symbol, number, or string", false
}

func (p *parser) next() {
	p.tok = p.s.Scan()
	p.lit = p.s.TokenText()
}

func (p *parser) expect(tok rune) {
	if p.tok != tok {
		if p.tok != EOF {
			panic("Syntax Error -- " + TokenString(tok) + " expected")
		}
		panic(IncompleteExpression(""))

	}
	p.next()
}
