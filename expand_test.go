package main

import (
	"testing"
)

func parse(s string) expression {
	e, ok := Parse(s)
	if !ok {
		panic("parse error: " + s)
	}
	return e
}

func TestExpand(t *testing.T) {
	env := defaultEnv()
	input := "(and a b)"
	expanded := print(expand(parse(input), env))
	if expanded != "(cond (a (cond (b T) (T NIL))) (T NIL))" {
		t.Errorf("expand(%v) -> %v failed", input, expanded)
	}
}
