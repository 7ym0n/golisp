(defun null (x) (equal x NIL))
(defun not (x) (cond (x NIL) (T T)))

; macro : (or a ..)
; (or) = NIL
(defmacro or (x)
  (cond ((null (cdr x)) 'NIL)
	(T (list 'cond (list (cadr x) 'T) (list 'T (cons 'or (cddr x)))))))

; macro : (and a ..)
; (and) = T
(defmacro and (x)
  (cond ((null (cdr x)) 'T)
	(T (list 'cond (list (cadr x) (cons 'and (cddr x))) (list T 'NIL)))))

(defun reduce (init args fun) 
  (cond ((null args) init) 
         (T (reduce (fun init (car args)) 
                          (cdr args) 
                          fun))))

(defun + args (reduce 0 args builtin.add))
(defun * args (reduce 1 args builtin.mul))
(defun / args 
  (cond ((null (cdr args)) (builtin.div 1 (car args))) 
    (T (reduce (car args) (cdr args) builtin.div))))
(defun - args 
  (cond ((null (cdr args)) (builtin.sub 0 (car args)))
    (T (reduce (car args) (cdr args) builtin.sub))))

(defun compareAll (args fn)
  (cond ((null (cdr args)) T)
    (T (and (fn (car args) (cadr args)) (compareAll (cdr args) fn)))))

(defun = args (compareAll args builtin.eq))
(defun > args (compareAll args builtin.gt))
(defun < args (compareAll args builtin.lt))

;; maplist transforms a list into another
;;  l : a list
;;  f : a transform function which takes a head of list l
(defun maplist (l f)
        (cond
                ((null l) NIL)
                (T (cons (f l) (maplist (cdr l) f)))))

;; map 
;;  transform each element into fn(e)
(defun map (l f)
  	(maplist l (lambda (x) (f (car x)))))

(defun gensym ()
  'gen-symbol) ; 

; macro: (select p (p1 e1) (p2 e2) e)
; ((lambda (x) (cond ((equal x p1) e1)
;                  ((equal x p2) e2)
;                  (T e))) p)
(defmacro select (form)
  ((lambda (symbol) (list (list 'lambda (list symbol) 
    (cons 'cond 
      (maplist (cddr form) 
        (function (lambda (l) (cond 
            ((null (cdr l)) 
              (list 'T (car l))) 
            (T 
              (list (list 'equal symbol (caar l)) (cadar l))))))))) (cadr form)))
   (gensym)))

